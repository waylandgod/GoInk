package hxgo

import (
	"net/url"
)

//-------------------------------

// input object
type Input struct {
	FormValues url.Values
}

// get new *Input from a *Request
func (this *Request) Input() *Input {
	this.Raw.ParseForm()
	return &Input{this.Raw.Form}
}

// get input data from key string
func (this *Input) Get(key string) string {
	return this.FormValues.Get(key)
}

// get input data slice from key string, sometimes checkbox or select can be multi-value
func (this *Input) GetSlice(key string) []string {
	vs, ok := this.FormValues[key]
	if !ok || len(vs) == 0 {
		return []string{}
	}
	return vs
}

// get all input data map as map[string]string, not support slice data
func (this *Input) All() map[string]string {
	m := make(map[string]string)
	for k, _ := range this.FormValues {
		m[k] = this.FormValues.Get(k)
	}
	return m
}

// get all input data without given keys
func (this *Input) Except(v...string) map[string]string {
	all := this.All()
	for _, n := range v {
		delete(all, n)
	}
	return all
}

// get mapped input data, given a new map reflection with map[new name]key
func (this *Input) Map(m map[string]string) map[string]string {
	data := make(map[string]string)
	for k, v := range m {
		data[k] = this.Get(v)
	}
	return data
}


